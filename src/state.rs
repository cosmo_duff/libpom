#[cfg(feature = "serde")]
use serde_crate::{Deserialize, Deserializer, Serialize};

use std::{
    fmt,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
    thread,
    thread::JoinHandle,
    time::{Duration, Instant},
};

use crate::{error::PomError, phase::Phase, settings::Settings};

#[derive(Debug, PartialEq, Eq)]
pub(crate) enum TimerResult {
    Completed,
    Cancelled,
}

/// Pomodoro state.
#[derive(Debug, Copy, Clone)]
#[cfg_attr(
    feature = "serde",
    derive(Serialize, Deserialize),
    serde(crate = "serde_crate")
)]
pub struct State {
    /// Instant for when a phase was started.
    #[cfg_attr(
        feature = "serde",
        serde(
            skip_serializing,
            deserialize_with = "instant_now",
            default = "Instant::now"
        )
    )]
    pub(crate) start: Instant,
    /// The remaining time for the current phase.
    pub(crate) remaining: Duration,
    /// The current phase of the pomodoro (pomodoro, break, long break).
    pub(crate) phase: Phase,
    /// The number of completed rounds.
    pub(crate) completed_rounds: u64,
}

impl fmt::Display for State {
    /// Prints the state in the format *phase*|*minutes*|*seconds*|*round*
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let remaining = self.remaining.as_secs();
        let min = remaining / 60;
        let sec = remaining % 60;
        write!(
            f,
            "{}|{:02}:{:02}|{}",
            self.phase, min, sec, self.completed_rounds
        )
    }
}

impl State {
    /// Returns the current phase of the state.
    pub fn phase(&self) -> Phase {
        self.phase
    }

    /// Returns the remaining time of a pomodoro as a std::time::Duration object.
    pub fn remaining_duration(&self) -> Duration {
        self.remaining
    }

    /// Returns the remaining time of the current phase in seconds.
    pub fn remaining_as_secs(&self) -> u64 {
        self.remaining.as_secs()
    }

    /// Returns the number of completed Pomodoro (work) rounds.
    pub fn completed_rounds(&self) -> u64 {
        self.completed_rounds
    }

    pub(crate) fn start(settings: Settings) -> State {
        State {
            start: Instant::now(),
            remaining: settings.pomodoro_length,
            phase: Phase::Pomodoro,
            completed_rounds: 0,
        }
    }

    pub(crate) fn next_round(&self, settings: Settings) -> State {
        let mut state = self.clone();
        if state.phase == Phase::Pomodoro {
            state.completed_rounds += 1;
            if state.completed_rounds == settings.rounds {
                state.completed_rounds = 0;
                state.phase = Phase::LongBreak;
                state.remaining = settings.long_break_length;
            } else {
                state.phase = Phase::Break;
                state.remaining = settings.break_length;
            }
        } else {
            state.phase = Phase::Pomodoro;
            state.remaining = settings.pomodoro_length;
        }
        state.start = Instant::now();

        state
    }

    pub(crate) fn remaining(&self) -> Option<State> {
        let mut state = self.clone();
        let remaining = state.remaining.checked_sub(state.start.elapsed());
        match remaining {
            Some(time) => state.remaining = time,
            None => {
                #[cfg(feature = "logging")]
                error!("Failed to calculate remaining time");
                return None;
            }
        }

        Some(state)
    }

    fn timer(&self, running: Arc<AtomicBool>) -> Result<JoinHandle<TimerResult>, PomError> {
        let state = self.clone();
        match thread::Builder::new()
            .stack_size(256)
            .name("timer".to_string())
            .spawn(move || {
                while state.start.elapsed() < state.remaining {
                    thread::sleep(Duration::from_millis(100));
                    if running.load(Ordering::Relaxed) {
                        continue;
                    } else {
                        return TimerResult::Cancelled;
                    }
                }
                return TimerResult::Completed;
            }) {
            Ok(thread) => return Ok(thread),
            Err(e) => return Err(PomError::ThreadFailed(e)),
        };
    }

    pub(crate) fn run(self, running: Arc<AtomicBool>) -> Result<TimerResult, PomError> {
        #[cfg(feature = "logging")]
        debug!("{:?}", self.phase);
        let timer = self.timer(running.clone())?;
        let result = match timer.join() {
            Ok(timer_res) => timer_res,
            Err(e) => {
                if let Some(e) = e.downcast_ref::<&'static str>() {
                    return Err(PomError::TimerPanic(e));
                } else {
                    static TIMER_ERROR: &str = "timer thread panicked from an unknown error";
                    return Err(PomError::TimerPanic(TIMER_ERROR));
                }
            }
        };

        Ok(result)
    }
}

impl Default for State {
    fn default() -> State {
        State::start(Settings::default())
    }
}

#[cfg(feature = "serde")]
fn instant_now<'de, D>(_deserializer: D) -> Result<Instant, D::Error>
where
    D: Deserializer<'de>,
{
    Ok(Instant::now())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[cfg(feature = "serde")]
    #[test]
    fn serialize_tst() {
        let state = State {
            start: Instant::now(),
            remaining: Duration::from_secs(60),
            phase: Phase::Pomodoro,
            completed_rounds: 0,
        };
        let json_str = serde_json::to_string(&state).unwrap();
        let _new_state: State = serde_json::from_str(&json_str).unwrap();
    }
}
