use std::convert::From;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::mpsc::channel;
use std::sync::mpsc::{Receiver, Sender};
use std::sync::Arc;
use std::thread;
use std::time::{Duration, Instant};

use crate::error::PomError;
use crate::settings::Settings;
use crate::state::{State, TimerResult};

/// Pomodoro struct containing the current state and settings.
///
/// A pomodoro can be built using seconds or minutes.
#[derive(Debug)]
pub struct Pomodoro {
    state: State,
    settings: Settings,
    running: Arc<AtomicBool>,
    pub(crate) external_sender: Option<Sender<State>>,
    internal_receiver: Option<Receiver<State>>,
}

impl Pomodoro {
    /// Create a default Pomodoro.
    pub fn new() -> Pomodoro {
        let settings = Settings::default();
        Pomodoro {
            state: State::start(settings),
            settings,
            running: Arc::new(AtomicBool::new(false)),
            external_sender: None,
            internal_receiver: None,
        }
    }

    /// Executes a Pomodoro.
    ///
    /// Starts the Pomodoro and returns the starting state.
    pub fn play(&mut self) -> Result<State, PomError> {
        #[cfg(feature = "logging")]
        info!("Play executed");
        // Executes the play if it is not already running
        if !self.running.load(Ordering::Relaxed) {
            // set running to true
            self.running.store(true, Ordering::Relaxed);
            // create channel for internal sending states
            let (sender, receiver) = channel();
            self.internal_receiver = Some(receiver);
            // Create copies of structs to be used in the event loop
            self.state.start = Instant::now();
            let mut state = self.state;
            let settings = self.settings;
            let running = self.running.clone();
            let ext_sender = self.external_sender.clone();
            // If the thread builder errors return a thread failed error
            if let Err(e) = thread::Builder::new()
                // create a thread and continuously run the state until the timer returns cancelled
                .name("play_thread".to_string())
                .spawn(move || loop {
                    let completed = state.run(running.clone());
                    match completed {
                        Ok(com) => {
                            if com == TimerResult::Completed {
                                state = state.next_round(settings);
                                sender.send(state).expect("Failed to send state.");
                                if let Some(ref s) = ext_sender {
                                    s.send(state)
                                        .expect("Failed to send state to external channel")
                                }
                            } else {
                                break;
                            }
                        }
                        Err(e) => {
                            #[cfg(feature = "log")]
                            error!("{}", e)
                        }
                    }
                })
            {
                return Err(PomError::ThreadFailed(e));
            }
            // return the starting state
            Ok(self.state)
        } else {
            // Return an error if the pomodoro is already running
            #[cfg(feature = "logging")]
            info!("Pomodoro is already running");
            Err(PomError::AlreadyRunning)
        }
    }

    /// Toggle a Pomodoro between play and pause or stop.
    ///
    /// The Pomodoro state after starting is returned.
    pub fn toggle(&mut self) -> Result<State, PomError> {
        if self.running.load(Ordering::Relaxed) {
            Ok(self.pause())
        } else {
            self.play()
        }
    }

    /// Pause a Pomodoro.
    ///
    /// The state of the pomodoro after executing pause is returned.
    pub fn pause(&mut self) -> State {
        self.running.store(false, Ordering::Relaxed);
        if let Some(ref recv) = self.internal_receiver {
            let iter = recv.try_iter();
            for state in iter {
                self.state = state;
            }
        }
        self.internal_receiver = None;
        if let Some(s) = self.state.remaining() {
            self.state = s;
        }
        #[cfg(feature = "logging")]
        info!("Paused: {}", self.state);
        self.state
    }

    /// Stop a Pomodoro.
    ///
    /// The state of the pomodoro after executing the stop is returned.
    pub fn stop(&mut self) -> State {
        self.running.store(false, Ordering::Relaxed);
        self.state = State::start(self.settings);
        #[cfg(feature = "logging")]
        info!("Stopped: {}", self.state);
        self.state
    }

    /// Get a Pomodoros current state.
    pub fn status(&mut self) -> State {
        if self.running.load(Ordering::Relaxed) {
            if let Some(ref recv) = self.internal_receiver {
                let iter = recv.try_iter();
                for state in iter {
                    self.state = state;
                }
            }

            let state = match self.state.remaining() {
                Some(round) => round,
                None => {
                    #[cfg(feature = "logging")]
                    warn!("Remaining failed to calculate. The state will not be updated.");
                    self.state
                }
            };
            #[cfg(feature = "logging")]
            info!("Status: {}", &state);
            return state;
        }
        #[cfg(feature = "logging")]
        info!("Status: {}", &self.state);
        self.state
    }

    /// Gets the pomodoro length.
    pub fn pomodoro_length(&self) -> Duration {
        self.settings.pomodoro_length
    }

    /// Gets the break length.
    pub fn break_length(&self) -> Duration {
        self.settings.break_length
    }

    /// Gets the long break length.
    pub fn long_break_length(&self) -> Duration {
        self.settings.long_break_length
    }

    /// Gets the number of pomodoros before a long break.
    pub fn rounds(&self) -> u64 {
        self.settings.rounds
    }
}

impl From<Settings> for Pomodoro {
    fn from(settings: Settings) -> Self {
        Pomodoro {
            state: State::start(settings),
            settings,
            running: Arc::new(AtomicBool::new(false)),
            external_sender: None,
            internal_receiver: None,
        }
    }
}
