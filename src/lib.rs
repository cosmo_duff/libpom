//!# Brief Overview
//!This crate provides a basic pomodoro as a library. It only uses the standard library only when
//!compiled with default features.
//!
//!# Setup
//!Add this to your `Cargo.toml`:
//!
//!```toml
//![dependencies]
//!libpom = "0.1.0"
//!```
//!
//!# Examples
//!This example creates a simple pomodoro runs it, sleeps for 5 seconds, pauses and then prints the
//!status.
//!
//!```
//!use std::thread;
//!use std::time::Duration;
//!use libpom::Pomodoro;
//!
//!fn main() {
//!    // create the pomodoro
//!    // it has a work time of 25 minutes a break time of 5 minutes and a long break of 15
//!    // a long break will be every 4 rounds
//!    let mut pom = Pomodoro::new();
//!
//!    // start the pomodoro
//!    pom.play();
//!
//!    thread::sleep(Duration::from_secs(5));
//!
//!    pom.pause();
//!
//!    println!("{}", pom.status());
//!}
//!```
//!
//!This example creates a pomodoro with all durations set to 1 minute using the builder method.
//!
//!```
//!use std::thread;
//!use std::time::Duration;
//!use libpom::PomodoroBuilder;
//!
//!fn main() {
//!    let mut pom = PomodoroBuilder::new()
//!        .pomodoro_length(1)
//!        .break_length(1)
//!        .long_break_length(1)
//!        .build();
//!
//!    pom.play();
//!
//!    pom.pause();
//!
//!    println!("{}", pom.status());
//!}
//!```
//!
//!This example creates a pomodoro using the builder pattern and passes in a sender. The receiving
//!end can be used to receive new states from the pomodoro.
//!
//!
//!```
//!use std::thread;
//!use std::sync::mpsc::channel;
//!use std::time::Duration;
//!use libpom::PomodoroBuilder;
//!
//!fn main() {
//!    let (sender, receiver) = channel();
//!    let mut pom = PomodoroBuilder::new()
//!        .pomodoro_length(1)
//!        .break_length(1)
//!        .long_break_length(1)
//!        .external_sender(sender)
//!        .build();
//!
//!    // move a continuous recv to another thread
//!    // we will just print any states received
//!    thread::spawn(move || loop {
//!        let state = receiver.recv().unwrap();
//!        println!("{}", state);
//!    });
//!
//!    pom.play();
//!
//!    pom.pause();
//!
//!    println!("{}", pom.status());
//!}
//!```
//!
//!## Crate features
//!
//!By default this crate only uses the standard library.
//!
//!+ **serde** -
//!    When enabled State can be serialized and deserialized.
//!+ **log** -
//!    Enables logging throughout the crate.

#[cfg(feature = "log")]
#[macro_use]
extern crate log;

/// Contains the Pomodoro state.
pub use pomodoro::Pomodoro;
pub use pomodoro_builder::PomodoroBuilder;
pub use state::State;
//pub use settings::Settings as Settings;
pub use error::PomError;
pub use phase::Phase;
mod error;
mod phase;
mod pomodoro;
mod pomodoro_builder;
mod settings;
mod state;

#[cfg(test)]
mod tests {
    use super::*;
    use std::thread;
    use std::time::Duration;

    #[test]
    fn run_pom() {
        let mut pom = PomodoroBuilder::new()
            .pomodoro_length(1)
            .break_length(1)
            .long_break_length(1)
            .build();

        println!("{:?}", pom);

        pom.play().unwrap();

        thread::sleep(Duration::from_secs(5));

        pom.pause();

        let status = pom.status();

        assert!(status.remaining < Duration::from_secs(60));

        pom.stop();

        let stop_stat = pom.status();

        assert!(stop_stat.remaining == Duration::from_secs(60));
    }

    #[test]
    fn toggle_test() {
        let mut pom = PomodoroBuilder::new()
            .pomodoro_length(1)
            .break_length(1)
            .long_break_length(1)
            .build();

        pom.toggle().unwrap();

        thread::sleep(Duration::from_secs(5));

        pom.toggle().unwrap();

        let state = pom.status();

        println!("{}", state);

        assert!(state.remaining < Duration::from_secs(60));
    }
}
